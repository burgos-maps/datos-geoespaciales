# Edificios construidos

Capa de edificaciones y construcciones del Mapa Topográfico de Castilla y León 1:5.000 con geometría de polígonos.
Mapa de entidades geográficas superficiales, referida al tema de la edificación y la construcción (según modelo de datos de elaboración de la cartografía). Formada por agregación de la información existente en las hojas de mapa de la serie Mapa Topográfico de Castilla y León 1:5.000.
Recopila cualquier clase de recinto constituido por construcciones cubiertas, que sean estables o que no lo sean. Se incluye también el fenómeno manzana.

## LICENCIAS Y ATRIBUCION

| Datos | Enlace Fuente | Licencia | EPGS |
| :------ |:--- | :--- | :--- |
| Junta de Castilla y León | http://www.idecyl.jcyl.es/geonetwork/srv/spa/catalog.search#/metadata/ES410JCLCITMTREDI05S20160301 | https://datosabiertos.jcyl.es/web/jcyl/RISP/es/Plantilla100/1284235967637/_/_/_ |EPSG:4326|

burgos:

http://opendata.jcyl.es/ficheros/carto/mapacyl/mapacyl5/burgos/shp_e5/
