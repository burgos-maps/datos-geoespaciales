# Edificios construidos

Capa de edificaciones y construcciones del Mapa Topográfico de Castilla y León 1:5.000 con geometría de puntos.
Mapa de elementos puntuales resultante de la agregación de todas las entidades geográficas pertenecientes al tema de la edificación y la construcción (según modelo de datos de elaboración de la cartografía) contempladas en las hojas de mapa de la serie Mapa Topográfico de Castilla y León 1:5.000.
Recopila cualquier clase de obra constructiva, del tipo depósito, silo, chimenea, monumento o grúa que, por sus reducidas dimensiones, exige ser capturado como fenómenos puntuales

## LICENCIAS Y ATRIBUCION

| Datos | Enlace Fuente | Licencia | EPGS |
| :------ |:--- | :--- | :--- |
| Junta de Castilla y León | http://www.idecyl.jcyl.es/geonetwork/srv/spa/catalog.search#/metadata/ES410JCLCITMTREDI05P20160301 | https://datosabiertos.jcyl.es/web/jcyl/RISP/es/Plantilla100/1284235967637/_/_/_ |EPSG:4326|

burgos:

http://opendata.jcyl.es/ficheros/carto/mapacyl/mapacyl5/burgos/shp_e5/
