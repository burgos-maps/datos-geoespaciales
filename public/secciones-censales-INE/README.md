# SECCIONES CENSALES


| Datos | Enlace Fuente | Licencia | EPGS |
| :------ |:--- | :--- | :--- |
| INE | http://www.ine.es/censos2011_datos/cen11_datos_resultados_seccen.htm | http://www.ine.es/ss/Satellite?L=0&c=Page&cid=1254735849170&p=1254735849170&pagename=Ayuda%2FINELayout |EPSG:4326|


### Dataset Original INE:

Contorno de las secciones censales a 1 de noviembre de 2011 en formato SHP (comprimido ZIP)

http://www.ine.es/censos2011_datos/cen11_datos_resultados_seccen.htm

### Subsets en formato .geojson:

[censo2011_BU.geojson](https://burgos-maps.gitlab.io/datos-geoespaciales/secciones-censales-INE/censo2011_BU.geojson)

[censo2011_CyL.geojson](https://burgos-maps.gitlab.io/datos-geoespaciales/secciones-censales-INE/censo2011_CyL.geojson)

### Campos destacados:

- CUSEC: CPRO+CMUN+CDIS+CSEC
- CSEC: seccion
- CDIS: distrito
- CMUN: codigo municipio
- CPRO: codigo provincia
- CCA: codigo comunidad autonoma

## basemap

CARTODB

https://github.com/CartoDB/basemap-styles

## LICENCIA INE

La información contenida en este sitio web procede de múltiples fuentes, por lo que el INE solo autoriza la reutilización de aquélla cuya fuente original sea el propio INE y siempre bajo las siguientes condiciones generales:

- Se prohíbe expresamente desnaturalizar el sentido de la información.
- Debe citarse la fuente de la información objeto de reutilización. Esta cita podrá realizarse de la siguiente manera: Fuente: Sitio web del INE: www.ine.es si no se realiza ningún tratamiento de los datos o bien: Elaboración propia con datos extraídos del sitio web del INE: www.ine.es en caso de que se realice tratamiento de los datos.
- Debe mencionarse la fecha de la última actualización de la información objeto de reutilización, siempre y cuando estuviera incluida en el original.
- No se podrá indicar, insinuar o sugerir que el INE participa, patrocina o apoya la reutilización que se lleve a cabo con la información.
- El INE no será responsable del uso que de su información hagan los agentes reutilizadores. Tampoco será responsable de los daños materiales o sobre datos, ni de posibles perjuicios económicos provocados por el uso de la información reutilizada.

Esta licencia de uso se rige por las leyes españolas independientemente del entorno legal del usuario. Cualquier disputa que pueda surgir en la interpretación de este acuerdo se resolverá en los tribunales españoles.

Cualquier duda o comentario sobre el contenido de este servidor debe dirigirse a la Subdirección General de Difusión Estadística del INE:

Formulario de consultas: www.ine.es/infoine, tlf: (+34) 91 583 91 00; fax: (+34) 91 583 91 58

http://www.ine.es/ss/Satellite?L=0&c=Page&cid=1254735849170&p=1254735849170&pagename=Ayuda%2FINELayout
