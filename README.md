## DATOS GEOGRAFICOS

### Licencia de la Junta de Castilla y León

[Licencia IGCYL](https://datosabiertos.jcyl.es/web/jcyl/RISP/es/Plantilla100/1284235967637/_/_/_)


### Agente que publica el dato

[Junta de Castilla y León](https://datosabiertos.jcyl.es/web/jcyl/RISP/es/PlantillaSimpleDetalle/1284162055979/_/1142233481734/DirectorioPadre?plantillaObligatoria=PlantillaContenidoDirectorio)